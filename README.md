WohaPlug
========
Wohaplug is an authentication plugin for bukkit, which utilizes the
simple api used on our server
[Gruvdrift](https://github.com/blambi/Gruvdrift).

This will replace wohasock since we have had some issues that might be
related to that approach.

Config
------
Currently there is only:

 - url: url to the web service.

Later on we will add secret key for authenticating post requests.

Permissions
-----------
None yet.

Missing features compared to wohasock
-------------------------------------

 - No MOTD
 - No Fortunes
 - No listing of online users.
 - Doesn't implement jailed as of yet.
 - Doesn't implement warnings as of yet.
